---
layout: post
date: 2016-07-06 12:00	
title: bc is a terrible calculator
published: true
---
I've been messing around with some terminal based calculators. For my
cryptography class, we had to do some huge computations. Ive noticed that
GNU bc is actually pretty slow at this:

    [whoishex@localhost ~]$ time echo "(2^2000000)%7" | bc
    4
    real    0m12.295s
    user    0m12.297s
    sys     0m0.000s
    [whoishex@localhost ~]$

Thats pretty slow! I tried bigger numbers and it would take minutes. In its
place, I've started using the python shell.

    [whoishex@localhost ~]$ time echo "(2**20000000)%7" | python -i 
    Python 3.5.2 (default, Jun 28 2016, 08:46:01) 
    [GCC 6.1.1 20160602] on linux
    Type "help", "copyright", "credits" or "license" for more information.
    >>> 4
    >>> 

    real    0m0.186s
    user    0m0.170s
    sys     0m0.017s
    [whoishex@localhost ~]$

basically instant. Gnome-calculator, wolfram alpha, and literally any other modern
calculator I can get my hands on is also equally fast. I think bc is trying to
calculate 2^2000000 before modding it out, while the other programs implement
some sort of [method of repeated squares](https://en.wikipedia.org/wiki/Modular_exponentiation#Memory-efficient_method). 
