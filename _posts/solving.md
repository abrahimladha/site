---
layout: post
date: 2019-01-06 23:49:00
title: A simple proof that took me a year.
published: false
---
A second problem I have recently had with puzzle cubes involves finding
solutions. 

A lot of people in the cubing community care about speed, but this is not
really something that I care about. I care about seeing if I can come up with a
solution for a cube independently. I think I managed to
accidentally solve the 5x5 once, but since I have been stuck on this issue.

picture of parity

When trying to come up with a solution for a new puzzle, the first thing I will
do is try and break it up into smaller problems. The easiest way to do this
with the cube puzzle is with a reduction. If you can group alike pieces
together, then pretend its a 3x3 and solve it from there. So solving an odd NxN
is really a problem of reducing it to a 3x3, then solving a 3x3. 

I like to think of the outside and inside layer as almost independent. If you
turn the outside layers, you will change nothing about the inside, but turning
the inside layers will change some of the outside. This can be abused. Push a
piece onto one layer, perform your moves freely without worrying about
destroying anything, and then pop it back into the other. There are analogies
to this in all kinds of math, like subspaces. 

To start, make the centers. This is not so hard until you have two faces left,
because you have to make them at the same time. 

After this, you form the edge pieces. The way I figured it out was this. You
want to move some edge piece next to another, so put them on the same face.
Then rotate the entire cube (destroying a lot in the process) to move the piece
in the right spot. Replace this edge with some other by moving it down and
swapping anything else back up, then fix the entire cube in the same way that
you destroyed it. Here is an example using a 5x5.

What this results in is that we have a simple algorithm we can perform to
permute any three same edge pieces. If we have solved every edge except for
three, then we can perform this to get to get all the edges solved, which we
can then solve using the 3x3 reduction. The problem though, is it usually ends
with a two edge pieces with each having the others piece. Applying this
algorithm will solve one and break two, only permuting the colors and leaving
us with the same problem. In order to create, we must destroy. We have to
introduce more unsolved edges in order to solve them more. 

I will use state diagrams in the form of cycles to illustrate where we are. I
know the last post was about how state diagrams might not be the best way to
model these kinds of problems but it works well here. Each node is an almost
completed edge except for one piece. The arrow from a node A to a node B
implies that the piece not belonging to the edge piece A actually belongs to B.
Because of the algorithm, we may freely delete or add in three-cycles. We may
also pick any three nodes and permute their pointers. Our goal is, using these
rules, to be able to delete every node. For example, this is where we are
currently stuck at. We will go through every case and count the possible
permutations

case 2: this is our dead end.
case 3: the only non degenerate case is the three cycle, which we know we can
delete
case 4: 
        four cycle
            By symmetry we can pick any three vertices to permute, so the
            choice does not actually matter. The result is the same and we are
            reduced back down to where we started
        2 2-cycle
case 5:
        2 cycle + 3cycle
        5 cycle
