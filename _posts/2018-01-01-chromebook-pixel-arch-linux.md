---
layout: post
date: 2018-01-01 12:00 
title: Chromebook Pixel and Arch Linux
published: true
---
I bought a Chromebook Pixel from 2013 during the Summer of 2016, since I wanted
a new laptop, and these had deprecated in value incredibly considering they
were $1200-1500 when they launched. I picked up the 64GB model for about $400. 
There are a lot of issues with this laptop and maybe I won't do the
same thing again. The display is incredibly bright and the image can often burn
in if left on for too long. There is basically no cooling on this machine, so
it was constantly at 85 celsius (I found a fix for this later) and the battery
life was pretty terrible. My old C720, I could easily get 12 or 13 hours out of
the thing. I have to charge this every day and be lucky to get 4 hours. Maybe I
was just spoiled. I do like the 4:3 aspect ratio. It makes having two windows
that partition the screen vertically more comfortable on a laptop. The
resolution is also somewhat wasted on me. The fonts I mostly use are usually
all monospaced and pixel perfect, so I don't see an upgrade for 95% of what I
do on this machine, but PDFs and webpages look like real paper, which is kinda
neat I guess.

![The machine](https://ladha.me/IMG/img3.jpg "The machine"){:height="576px" width="768px"}

There are two ways you can install linux on this machine. You can leave the
machine in developer mode and keep chromeos, or you can do a small hardware mod
and flash the bios to have a full UEFI ROM. I eventually moved to the UEFI
method since you get some nice things, like suspend-to-disk and no weird error
screen on boot. These instructions are specific to this now 5 year old laptop,
but they could work for any chromebook really.

We want to flash a UEFI ROM. First we need to remove a single R/W screw from
inside the machine. Power it off and flip it over. The back panel is held down
by 4 screws covered by the little rubber feet. You should have the proper tools
for this. I tried to remove them using a combination of butter knives and
melted pens and I ended stripping one of the miniscule screws. You should buy a
jewelers kit if you are serious enough.

![The back](https://ladha.me/IMG/img2.jpg "The back"){:height="576px" width="768px"}

They are very very tiny. After taking them out you should feel the backplate be kind of
loose. Use some kind of spudger tool to pry it and be careful of the clips near
the IO ports. I used a guitar pick for the gluey stuff and a plastic butter
knife for the clips. You might need to flex the back a little but be careful.
After the back is off look for a screw that looks like this near the USB ports:

![The screw](https://ladha.me/IMG/img1.jpg "The screw"){:height="1024px" width="768px"}

I didn't find any documentation where this screw was on the 2013 model, but
there were pictures of it on the 2015 model, and I removed it to see if it was
the same and sure enough I could write. It also has those telling little
contacts on it. It seems to be smaller than the R/W screw on other chromebooks.
After you remove this screw and the little contact washer on it, clip the back
panel back on and put the screws back in. I didn't have to glue the feet back on.
There was enough already left on the feet so they just stuck back in.

After we have removed the screw. Get to a chronos terminal window and run this
command:

     cd; curl -LO https://mrchromebox.tech/firmware-util.sh && sudo bash firmware-util.sh

Normally you should be worried about running random scripts off the internet,
but the worst that can happen is that it destroys your operating system. Which
you plan on doing anyway with it. Make sure you save everything you want from
your current chromeos install because doing this next step will nuke
everything. After the script runs you will be dropped in to some basic menu.
You want to select option 3, Installing the full ROM firmware. Follow the steps
and afterwards when you reboot you should be dropped into an intel EFI shell
with some very basic commands. Go make a USB of Arch and plug it in and reboot

I wanted to make the Instructions for installing Arch as easy as possible
simply by regurgitating commands. So thats what the next list of stuff is with
some included. I came up with this list from several guides modified to better
fit this machine.

    wifi-menu

Connect to your wifi network, then update, and check if the efi variables are non empty.

    pacman -Sy
    efivar -l
    lsblk
    gdisk /dev/sda

Once you are in gdisk, type x z y y to clear any all current partitions.

    cgdisk /dev/sda

We are going to make 3 paritions. A 512MiB root partition. an 8GiB swap
partition, and the remaining as our root partition. Adjust these values if you
want. I think a 4GiB swap size is fine. As long as its bigger than your 4GiB
ram if you want suspend properly.

boot: new, enter, first sector is blank, size is 512MiB, hex code is EF00, name
is boot

swap: new, enter first is blank second size is 8GiB, hex code is 8200, name is
swap

root: new, enter, first is blank, size is blank (remaining), hex code is blank,
name is root

You want to write those changes and then exit cgdisk. Now we format our partitions:

    mkfs.fat -F32 /dev/sda1
    mkswap /dev/sda2
    swapon /dev/sda2
    mkfs.ext4 /dev/sda3
    mount /dev/sda3 /mnt
    mkdir /mnt/boot
    mount /dev/sda1 /mnt/boot
    cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlistbackup
    sed -i 's/^#Server/Server/' /etc/pacman.d/mirrorlistbackup
    rankmirrors -n 6 /etc/pacman.d/mirrorlistbackup >> /etc/pacman.d/mirrorlist
    pacstrap -i /mnt base base-devel #should download a gigabyte
    genfstab -U -p /mnt >> /mnt/etc/fstab
    arch-chroot /mnt
    uncomment en_US UTF-8 from /etc/locale.gen
    locale-gen
    echo LANG=en_US.UTF-8 > /etc/locale.conf
    export LANG=en_US.UTF-8
    ln -s /usr/share/zoneinfo/America/New_York > /etc/localtime
    hwclock --systohc --utc

My hostname is "pixel". You can change yours as well.

    echo pixel > /etc/hostname

We are also going to enable trim on this since it technically has an SSD. This
will run once a week.
    
    systemctl enable fstrim.timer

in /etc/pacman.conf enable multilib by uncommenting [multilib] and the next line
    
    pacman -Sy
    passwd

now to set up our user:
    
    useradd -m -g users -G  wheel,storage,power -s /bin/bash whoishex
    visudo

uncomment the wheel line
add to the end of the file ```Defaults rootpw```
    
    mount -t efivarfs efivarfs /sys/firmware/efi/efivars/ 

This should say its already mounted. The UEFI ROM does not find where ever grub puts its grubx64.efi file, and there
is a bug where changes to where to find the EFI file do not persist in the UEFI
settings. I have had much more luck with systemd-boot in it getting picked up.

    bootctl install
    pacman -S intel-ucode

in the file ```/boot/loader/entries/arch.conf```

    title Arch Linux
    linux vmlinuz-linux
    initrd /intel-ucode.img
    initrd /initramfs-linux.img

Exit the file and save it.
    
    echo "options root=PARTUUID=$(blkid -s PARTUUID -o value /dev/sda3) rw" >> /boot/loader/entries/arch.conf

You can open up the arch.conf again to see what this did.
We need to find the name of our networking device. Mine is called wlp3s0. You
might have another device if you have an LTE card so enable that as well.
    
    ip link
    systemctl enable dhcpcd@wlp3s0.service
    pacman -S networkmanager
    systemctl enable NetworkManager.service
    exit
    umount -R /mnt
    shutdown now

Now unplug the drive and power on. From here you can install your desktop enviroment or window manager. Make sure
you install a display manager like lightdm or gdm so you can login easier.

To get suspend working edit the file in ```/boot/loader/entries/arch.conf ``` to say ```options resume=/dev/sda2 root...```
change ```HandleLidSwitch=hibernate``` in ```/etc/systemd/logind.conf```

If you want to control the lightbar and fans and thermals. There is a utility
called ```ectool```. I happened to find a binary version of it [here](https://github.com/abrahimladha/ectool-chromebook-pixel-2013). Move it to ```/bin/```
and type ```ectool help ``` to get started with it.

You might have some issues with suspending twice.
[This](https://plus.google.com/+LinusTorvalds/posts/UCoQzy8g2xB) is the fix for
that. 
