---
layout: post
date: 2016-07-06 12:00	
title: The Chromebook Pixel 2013, a linux users review
published: false
---
So after my c720 running out of memory and not having swap set up for what
feels like the hundreth time i felt the need to upgrade to a better laptop. I
wanted to get something with good free software support, including at the bios
level, so coreboot was a priority, so I ended up on this. I picked it up for
400 dollars for the 64gb LTE 2013 model. 

hardware

So The first and most noticable part of this laptop is the screen. A 12.85
2560 x 1700 3:2 display. The aspect ratio makes it great for programming. It
keeps the laptop still compact while maximizing screen real estate. The sheer
amount of pixels also make the web look amazing, but for programming I use a
perfect pixel font, so theres no difference for that. The biggest downside for
the screen is the extreme glare. Its a touchscreen so they used a glass panel.
Glass screens are already commonly known for their glare, but this is another
level. Its significantly more than my phone, or my friends macbooks. As I am
typing this, I can see my face in the terminal with as much detail as a mirror.
The screen also extremely bright and has significant burn in issues.
I leave the screen anywhere from 5 to 15% brightness since anything else pains
my cornea. Images also burn in easily. If I leave something thats not solid
black open for a few minutes, the pixels preserve the color slightly and the
image persists. Its not a super big issue since terminal backgrounds are black
anyway, and I use a dark GTK theme anyway.

The keyboard is nice, and simple. Theres only a ctrl and alt modifier. No
windows key. no fn key. no caps lock key. Typing on it is pretty good I guess.
I think most laptop keyboards are fine except those new ultrabook keyboards
which are garbage. Theres also a windows modifier where the caps lock key
should be so binding that to escape is great for vim. The escape and function
keys are a little harder to press and kind of hinge instead of depressing. Not
that big of a deal but just something to notice. 

The specs are nice too. 64GB storage, 4GB ram, some i5 chip. The problem seems
to be that since this is google's first laptop, they just tried to copy some
popular features of other common laptops with out really understanding them on
a deeper level. For example theres no vents in this thing at all, but theres
still a fan, so the exhaust ends up behind the spaces between keys on the
keyboard. Anything mildly graphically intensive spikes the temperature to
85-100 degrees celsius and it blows directly over the WASD keys burning my
hand. The exterior of the laptop is also aluminum colored gray, but its stamped
instead of milled. This provides absolutely none of the structural benefits and
all of the downsides as how efficient it is for transmitting heat. 

software

First thing I did was wipe chromeos entirely from the drive and reformat the
drive as one partition. I am running my love-hate relationship with arch linux.
I love it because the package manager is fast and it allows unlimited
customization. You get to build it up rather than tear something apart. I hate
it because something always breaks and I spend way too much time trying to fix
it. The interesting part about this laptop is since it is such a rare device
for anyone to own, linux support is spotty. I had to compile and implement the
keyboard backlight from some random repo on github myself. After months, I
still haven't gotten it to hibernate to my swapfile when I close the lid, and I
have accepted that I never will. 

The high dpi of this display means its supposed to be run in hidpi mode. This
seems to work more than fine on gnome stuff, but I run i3wm. configuring hipdi
on arch linux without a desktop enviroment is still a pain. When I log in, it
forgets what the dpi is until I reset it. Changing the display resolution in
arandr (to make some graphical stuff not scream the laptop) does not change the
dpi, so I end up with a giant cursor. Some apps simply ignore everything, all
xrandr presets and gtkrc and qt configs and just render however they want.
This experience makes any not default usecase painful. 
