---
layout: post
date: 2018-12-12 02:06:00
title: Math over Magic
published: false
---
I recently came across the following meme:



It got me thinking. Languages can vary 


I remember being in preK learning about addition, and one thing I was
consistently stuck on was the fact that you can only add two numbers at a time.
If I give you $$a +b+c$$, you have to either do $$(a+b)+c$$ or $$a + (b+c)$$
first. This goes for every operator I have seen. Operators are just functions.
We can write $$a+b$$ as $+(a,b)$. This made me realize that every function that
I have ever written down has $$f(a,...)$$ on the LHS and then a composition of
binary operators on the right hand sign. For example, $$+(a,b,c) = +(+(a,b),c)$$.

This lead me to an obvious question. Can there exist a function of three or
more variables which cannot be decomposed into functions of two variables? I
couldn't think of an example from anything. From calculus, integrals are just
summands, which are additions. Then looked to geometry, but all the formulas
can still be broken down. For example, if the three inputs are points, and the
function is to compute the area of a triangle, then we can just use Heron's
formula. My friend matthew recommended permutations, for example swapping three
objects in a rotation. The problem with this is that every permutation can be
broken down into a composition of 2-cycles. It did not seem possible.  


