---
layout: post
date: 2018-12-12 02:06:00
title: Math over Magic
published: true
---
The world is a lot more boring when you realize somethings can't exist. 

### Vampires ###


Take for example vampires. Vampires are mythical creatures who are immortal and suck
the blood of humans. The people they consume end up turning into more vampires. It
differs a lot from lore to lore and I don't really care to read more about
them. Lets assume (to the contrary) that a single vampire exists, and they feast at a
constant rate of once a month, and that all the victims they feast on do turn
into vampires. At the first month, there is one vampire, and they feast on a
human. For the second month, there are now two vampires, and they both feast so
for the third month we have four vampires. We can model the number of vampires
on the planet during month $$n$$ as:

\begin{equation}
f(n) = 2^{n-1}
\end{equation}

Lets take a look at three years since we have one vampire, for $$n = 12 \cdot 3
= 36$$, the number of vampires is $$f(n) = 2^{n-1} = 2^35 = 34,359,738,368$$. So in three years
we would have 34 billion vampires. But how can this be when theres only 8
billion people on the planet? All of the population would have to be vampires.
Since I am not a vampire, then vampires cannot exist. 

This argument also works for zombies and other things that can behave virally
and unrestricted. You can play with the numbers but the outcome will be the
same. Have the vampires feed once a year or once a lifetime, eventually the
whole planet would become vampires. 

### Pinocchio Computer ###


Pinocchio is a wooden boy who is cursed in such a way that if he tells a lie,
his wooden nose will grow. Here we have a physical, measurable
event dependent on the truth value of a logical statement. We can use him as a
computer then. Given a statement, his nose will either grow, or not grow. He
decides every problem. [But we know such a computer cannot
exist](https://en.wikipedia.org/wiki/Entscheidungsproblem). Therefore,
pinochio cannot exist. 

There are some issues with this argument. You could argue this only works for
logical statements that Pinocchio knows the truth value of, so if given a
statement, he might simply reply "I don't know" or something. In one of the
shrek movies, they are interrogating him and he does not want to reveal some
information, so he simply provides non-answers. You could also
argue that even though our Pinocchio computer will always halt (because we said
it decides) we have no idea when it will halt. We might die before it happens.
Here we kind of jumped to using the small wooden boy as a universal computer,
when it would have been enough just to feed him an undecidable problem. Ask him
to prove some self referential statement, or determine if some other machine
will halt.


### The Earth is round ###


I got into an argument with some people who believe that the earth is flat.
This made me kind of realize that I am not entirely certain why the earth is
round. Obviously it is, the ships over the horizon, the Erosthanese experiment.
Flat earthers don't even seem certain about their model. They are only certain
enough to deny that the earth is round. I wanted to see if I could come up with
a proof that the earth is round using as few physical assumptions as possible.
Flat earthers will even deny gravity(universal acceleration) as a force. There
are a few ways you could realize that planets have to be round. Lets say you agree
potential energy is a thing, and will naturally be minimized. Given any object
in 3-space, lets say their are two points on it of different height. The
kinetic energy here is linear in the difference in height ($$E_p = mgh$$), and it is zero if
they are the same height. Since the object will minimize potential energy over
enough time, all points on the surface of this object will be equidistant from
some center point. This is literally the definition of a sphere. 

Another argument involved minimizing surface area, so you have the
[isoperimetric
inequality](https://en.wikipedia.org/wiki/Isoperimetric_inequality) as just an equality which in 3 space is again, just a sphere. 
