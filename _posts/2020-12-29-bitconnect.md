---
layout: post
date: 2020-12-29 02:06:00
title: The Bitconnect Story
published: true
---

For the bitcoin class, I usually get to give one lecture. I use this time to
present some blockchain case studies. Specifically, this story. 

I was playing league of legends, and I was ADC. Normally, I am support, but I
got autofilled. I was really bad, but my support was this random maokai who was
really good and helping me a lot. After the game he added me. Months later, I
noticed his status message was about bitcoin, so I decided to message him about
it. We got into a kind of big discussion. Eventually he sent me an affiate link
for me to buy bitconnect. I could tell pretty easily it was a ponzi scheme scam
thing, but I humored him. 

![league1](https://ladha.me/IMG/league1.png "league1"){:height="576px" width="768px"}
![league2](https://ladha.me/IMG/league2.png "league2"){:height="576px" width="768px"}

Eventually, he started talking about making his own coin, and he wanted my
help. One of those "idea" guys with no actual technical skill, just wanted to
be in charge. I knew the type well. Anyway I said of course, maybe I could
write some really short code and walk away with my rent paid. It was somehow
related to solar energy. I don't exactly remember. We exchanged info on
facebook and chatted a bit more. His facebook was full of photos traveling to
all kinds of places like Dubai, and staying in fancy hotels. He again
encouraged me to invest in bitconnect. He said a big conference was coming up,
and that would raise the price, making me a quick buck.

![guyfb](https://ladha.me/IMG/guyfb.png "guyfb"){:height="288px" width="768px"}

I didn't know it at the time, but that event would become legendary. Not for
the reasons he wanted. That conference was where the [bitconnect meme](https://www.youtube.com/watch?v=lCcwn6bGUtU) came from.
He posted a lot of photos. 

Like all ponzi schemes, it has to bust. I messaged him, asking what happened,
then he blocked me.

Much later I came across his name in a lawsuit! I thought he was just some guy
who got burned, but no. He was at the very top. He is listed as "director".
among a few other people. A few other sources have him listed as co-founder. He
has himself listed as "Regional Vice President". [Here is a detailed article on
him in some weird alien language.](https://www.lapresse.ca/actualites/justice-et-faits-divers/actualites-judiciaires/201802/02/01-5152369-dirigeant-dune-entreprise-frauduleuse-ou-victime-dun-vol-didentite.php) [Here is a copy of one lawsuit.](http://securities.stanford.edu/filings-documents/1064/B00_01/2018619_r01k_18CV80086.pdf)  There are a
few in a couple states and countries. Its crazy that I found this guy as a
complete random in a videogame. 

This whole blog post could have been a tweet. Speaking of, I now have made a
twitter. Follow me at [@abrahimladha](https://twitter.com/abrahimladha). I don't yet have anything to tweet about.
