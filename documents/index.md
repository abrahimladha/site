---
layout: page
title: Documents
---
### Papers

2022-03 [Auditable, Available and Resilient Private Computation on the Blockchain via MPC](https://eprint.iacr.org/2022/398)

2020-05 [The GABLE Report](https://cfwebprod.sandia.gov/cfdocs/CompResearch/docs/GABLE-report-1v0.pdf)

2015-05 [Exploring mod2 n-queens games](https://content.sciendo.com/view/journals/rmm/6/11/article-p15.xml?lang=en)


### Other slides, recordings, notes

2021-10 [Information Theoretic Cryptography](https://www.youtube.com/watch?v=fvChVJHw0ok)

2021-09 [The NSA and Cryptography](https://www.youtube.com/watch?v=10Wl4ZRorrA) and [slides](https://ladha.me/files/NSA.pdf)

2021-06 [Automata and Complexity Lectures and Notes](https://cryptolab.gtisc.gatech.edu/ladha/CS4510SU21.html)

2021-05 [Section X worksheets](https://ladha.me/files/sectionX)

2020-09 [A Direct Appeal to Intuition](https://ladha.me/files/fancy-turing-notes.pdf)

2020-02 [CRAQ](https://ladha.me/files/CRAQ.pdf)

2019-11 [Quantum Secret Sharing](https://ladha.me/files/qss.pdf)

2019-11 [Post Quantum Crypto and Learning with Errors](https://ladha.me/files/nist.pdf)

2019-10 [Post Quantum Cryptography](https://ladha.me/files/pq_crypto.pdf)

2019-04 [Topological Quantum Computation](https://ladha.me/files/top-comp.pdf)

2019-03 [Bitcoin Class Lecture](https://ladha.me/files/bitcoin313.pdf)

2018-12 [The Word Problem for Groups and Applications to Knot Theory](https://ladha.me/files/knot_theory_word_problem.pdf)

2018-10 [Provable Security and Cryptocurrency, Big O Theory Club](https://theoryclub.github.io/2018/security-cryptocurrency)

2016-12 [The Ethereum Scratch off Puzzle](https://arxiv.org/abs/1612.04518)

2016-07 [A Secure Set Intersection Protocol using Hamming Distance as a Comparator](https://ladha.me/files/writeup.pdf)

2016-04 [Hypothetical Problems concerning the Theory of Relativity on Cryptographic Currency Implementations](https://arxiv.org/abs/1604.04265)

### Non Academic

2014-09 [CONVERGENCE ZINE SEPTEMBER 2014: GLITCH](https://ladha.me/files/September2014.pdf)

2011-2015 [Several indie games I made](https://vgdesign.itch.io/)
