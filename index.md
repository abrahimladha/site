---
layout: page
title: About Me
#date: 2014-10-19 23:51
---
Hello. My name is Abrahim Ladha. I am a lecturer with the College of Computing at Georgia Tech. I teach complexity and algorithms courses. My interests are across theoretical computer science, including complexity, computability, logic, algorithms, cryptography, combinatorics, and quantum computing.

You can contact me at <a href="mailto:abrahimladha@gatech.edu">abrahimladha@gatech.edu</a>. I am also on [Mastodon](https://mathstodon.xyz/@abrahimladha)

[CS4510 Summer](https://cryptolab.gtisc.gatech.edu/ladha/CS4510SU23.html)

[CS3510](https://cryptolab.gtisc.gatech.edu/ladha/CS3510S23.html)

[CS4510](https://cryptolab.gtisc.gatech.edu/ladha/CS4510S23.html)



![me](https://ladha.me/IMG/me.jpg)


